const navBtn = document.querySelector('.nav-mobile__btn');

navBtn.addEventListener('click', () => {
    const navList = document.querySelector('.nav-list');
    if(navList.classList.contains('active')) {
        navList.classList.remove('active');
        navBtn.classList.remove('active');
    } else {
        navList.classList.add('active');
        navBtn.classList.add('active');
    }
});

const userMenu = document.querySelector('.user');

if(userMenu) userMenu.addEventListener('click', () => {
    let userList = document.querySelector('.user-list');

    if(userList.classList.contains('active')) {
        userList.classList.remove('active');
    } else {
        userList.classList.add('active');
    }
})